import axios from 'axios';
import React from 'react'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2';
import Absen from './Absen'
export default function Presensi() {

  const history = useHistory();

  const absenMasuk = async (user) => {
    console.log(user);
    await axios.post(`http://localhost:3007/absen/absen-masuk?keterangan=${"MASUK"}&userId=${localStorage.getItem("userId")}`);
    history.push("/presensi");
    Swal.fire({
        icon: 'success',
        title: 'Berhasil Absen',
        showConfirmButton: false,
        timer: 1500
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
  
}
  const absenPulang = async (user) => {
    console.log(user);
    await axios.post(`http://localhost:3007/absen/absen-pulang?keterangan=${"PULANG"}&userId=${localStorage.getItem("userId")}`);
    history.push("/presensi");
    Swal.fire({
        icon: 'success',
        title: 'Berhasil Absen',
        showConfirmButton: false,
        timer: 1500
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
  
}

  return (
    <div className="container my-5 bs">
         <a className='btn btn-warning' style={{textAlign: "center"}} aria-current="page" href="/home"><i class="fas fa-backward"></i> Kembali</a>
                <br />
                <br />
               <a className="group block h-56 w-96" onClick={absenMasuk}>
  <div
    className="relative flex h-full items-end rounded-3xl border-4 border-black bg-white p-8 transition group-hover:-translate-x-2 group-hover:-translate-y-2 group-hover:shadow-[8px_8px_0_0_#000]"
  >
    <div className="lg:group-hover:absolute">
      <span aria-hidden="true" role="img" className="text-3xl sm:text-4xl">📆</span>
      <p className="mt-4 text-xl font-bold sm:text-2xl">Absen Masuk</p>
    </div>
  </div>
</a>
<br />
<a className="group block h-56 w-96" onClick={absenPulang}>
  <div
    className="relative flex h-full items-end rounded-3xl border-4 border-black bg-white p-8 transition group-hover:-translate-x-2 group-hover:-translate-y-2 group-hover:shadow-[8px_8px_0_0_#000]"
  >
    <div className="lg:group-hover:absolute ">
      <span aria-hidden="true" role="img" className="text-3xl sm:text-4xl">📆</span>
      <p className="mt-4 text-xl font-bold sm:text-2xl">Absen Pulang</p>
    </div>
  </div>
</a>
<br />
<div>
  <Absen />
</div>
    </div>
  )
}

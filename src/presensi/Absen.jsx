import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Swal from 'sweetalert2';

export default function Absen() {
  const [absen, setAbsen] = useState([]);
  const [totalpages, setTotalPages] = useState([]);

  const history = useHistory();

  const getAll = async (page = 0) => {
    await axios
      .get(
        `http://localhost:3007/absen?page=${page}&userId=${localStorage.getItem("userId")}`
      )
      .then((res) => {
        const pages = [0, 1];
        for (let index = 0; index < res.data.data.totalpages; index++) {
          pages.push(index)
        }
        setTotalPages(pages)
        setAbsen(res.data.data)
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll();
  }, []);

  const deleted = async (id) => {
    // Async-await bisa dikatakan sebagai cara mudah menggunakan JavaScript Promise yang agak sulit dipahami.

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3007/absen/" + id)
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )

      }
    }).then(() => {
      window.location.reload();
    });
  }

  return (
    <div>
      <div className="overflow-x-auto text-center">
        <table className="min-w-full divide-y-2 divide-gray-200 text-sm w-full">
          <thead>
            <tr>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                No
              </th>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                Nama
              </th>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                Keterangan
              </th>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                Jam
              </th>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                Tanggal
              </th>
              <th
                className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
              >
                Aksi
              </th>
            </tr>
          </thead>

          <tbody className="divide-y divide-gray-200">
            {absen.map((data, idx) => {
              return (
                <tr className="odd:bg-gray-50" key={idx}>
                  <td>{idx + 1}</td>
                  <td>{data.userId.username}</td>
                  <td>{data.keterangan}</td>
                  <td>{data.jam}</td>
                  <td>{data.tanggal}</td>
                  <td>
                    <button
                      className="btn btn-danger cursor-pointer"
                      style={{ backgroundColor: "red" }}
                      onClick={() => deleted(data.id)}
                    >
                      <i class="fas fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              )
            })}

          </tbody>
        </table>

        <br />
        <br />

        <Container className="pag">
          <Pagination aria-label="Page navigation example">
            <PaginationItem>
              <PaginationLink
                first
                href="#"
              />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink previous />
            </PaginationItem>
            {
              totalpages.map((data, index) => (
                <PaginationItem key={index}>
                  <PaginationLink onClick={() => getAll(data)}>
                    {data + 1}
                  </PaginationLink>
                </PaginationItem>
              ))
            }

            <PaginationItem>
              <PaginationLink next />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink
                href="#"
                last
              />
            </PaginationItem>
          </Pagination>
        </Container>
      </div>
    </div>
  )
}

import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom';
import { Form, InputGroup } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function EditProfil() {
    const param = useParams();
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [foto, setFoto] = useState("");
    const [alamat, setAlamat] = useState("");
    const [noTelepon, setNoTelepon] = useState("");

    const history = useHistory();

    useEffect(() => {
        axios.get("http://localhost:3007/acount/" + param.id)
        .then((response) => {
            const newAkun = response.data.data;
            setUsername(newAkun.username);
            setEmail(newAkun.email);
            setPassword(newAkun.password);
            setFoto(newAkun.foto);
            setAlamat(newAkun.alamat);
            setNoTelepon(newAkun.noTelepon);
        })
        .catch((err) => {
            alert("User id tidak ada " + err)
        })
    }, [])

    const editProfil = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("username", username)
        formData.append("email", email)
        formData.append("password", password)
        formData.append("file", foto)
        formData.append("alamat", alamat)
        formData.append("role", "USER")
        formData.append("noTelepon", noTelepon)
        Swal.fire({
                title: 'apakah yakin mau di edit profilnya?',
                showCancelButton: true,
                confirmButtonText: 'Edit',
      }).then((result) => {
        if (result.isConfirmed) {
         axios.put("http://localhost:3007/acount/" + param.id , formData, 
       {
        headers: {
          Authorization:`Bearer ${localStorage.getItem("token")}`
        }
          }
          ).then(() => {
            history.push("/profil");
            Swal.fire('Berhasil Mengedit!', '', 'success')
          }).catch((error) => {
            alert("terjadi kesalahan sir " + error)
          })
        }
      })
      }

  return (
    <div>
          <div className="edit mx-5">
        <div className="container my-5">
          <form onSubmit={editProfil}>
            <div className="name mb-3">
              <Form.Label>
                <strong>Nama</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Nama"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </InputGroup>
            </div>
  
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Email"
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)} />
              </InputGroup>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="password"
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)} />
              </InputGroup>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Foto profil</strong>
              </Form.Label>
              <div className="d-flex gap-3">
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                  required
                  type="file"
                    onChange={(e) => setFoto(e.target.files[0])} />
                </InputGroup>
              </div>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <div className="d-flex gap-3">
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="Address"
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)} />
                </InputGroup>
              </div>
            </div>
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Phone</strong>
              </Form.Label>
              <div className="d-flex gap-3">
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="Number"
                    value={noTelepon}
                    onChange={(e) => setNoTelepon(e.target.value)} />
                </InputGroup>
              </div>
            </div>
            <div className="d-flex justify-conten-end align-items-center mt-2 text-blue-600">
              <button className="buton btn" >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}


import axios from 'axios';
import React, { useState } from 'react'
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import { Form, InputGroup } from 'react-bootstrap'
export default function Register() {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [foto, setFoto] = useState("");
    const [alamat, setAlamat] = useState("");
    const [noTelepon, setNoTelepon] = useState("");

    const history = useHistory();

    const register = async (e) => {
        e.preventDefault();
        e.persist();
        const formData = new FormData();
        formData.append("file", foto)
        formData.append("username", username)
        formData.append("email", email)
        formData.append("password", password)
        formData.append("role", "USER")
        formData.append("alamat", alamat)
        formData.append("noTelepon", noTelepon)

        try {
            await axios.post("http://localhost:3007/acount/sign-up", formData, {

                headers: {
                    'Content-Type': 'multipart/form-data',
                },

            }).then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil Registrasi',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(() => {
                    history.push('/')
                }, 1250)
            })
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <div className="kotak_login">
            <h1 className="mb-5">Registrasi</h1>
            <Form onSubmit={register}>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Username</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="username"
                            type="text"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Foto Profil</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            type="file"
                            onChange={(e) => setFoto(e.target.files[0])}
                        />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Alamat</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="alamat"
                            type="text"
                            value={alamat}
                            onChange={(e) => setAlamat(e.target.value)}
                        />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Phone</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="Phone"
                            type="text"
                            value={noTelepon}
                            onChange={(e) => setNoTelepon(e.target.value)}
                        />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Email addres</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </InputGroup>
                </div>
                <div className="mb-3">
                    <Form.Label>
                        <strong>Password</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                        <Form.Control
                            placeholder="Password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </InputGroup>
                </div>
                <button variant="primary" type="submit" className="mx-1 button btn">
                    <i class="fas fa-sign-in-alt"></i>Register
                </button><br /><br />
                <center>
                    <a href="/">Login</a>
                    <br />
                    <span >Jika sudah memiliki akun</span>
                </center>
            </Form>
        </div>
    )
}

import React from 'react'
import Navbar from './Navbar'

export default function Home() {
  return (
    <div>
        <div>
          <Navbar />
        </div>
        <section className=" text-white text-center">
  <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
    <div className="mx-auto max-w-lg text-center">
      <h2 className="text-3xl font-bold sm:text-4xl">Presensi dan To Do List</h2>

      <p className="mt-4 text-gray-300">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur
        aliquam doloribus nesciunt eos fugiat. Vitae aperiam fugit consequuntur
        saepe laborum.
      </p>
    </div>

    <div className="p-10 max-md:pl-[8px] grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 mx-auto  mt-[3%]">
        <div className="rounded overflow-hidden shadow-lg w-[300px] h-[450px] mx-auto mt-[10px]  md:mr-[-1px] ease-in duration-200 hover:bg-gray-100">
          <a href="/presensi">
            <img
              className="w-[256px] mx-auto mt-[20px]"
              src="https://id1.dpi.or.id/uploads/images/2022/05/image_750x395_628cab6c628d2_1.jpg"
              alt="Mountain"
            />
            <br />
            <br />
            <div className="px-6 py-4">
              <div className="font-bold text-center text-xl mb-2">
                Presensi Harian
              </div>
              <p className="text-gray-700  text-center">
              presensi adalah sistem yang digunakan untuk menunjukkan kehadiran seseorang.
              </p>
            </div>
          </a>
        </div>
        <div className="rounded overflow-hidden shadow-lg  w-[300px] h-[450px] md:ml-[-1px] mt-[10px] mx-auto ease-in duration-200 hover:bg-gray-100">
          <a href="/todo">
            <img
              className="w-[256px] mx-auto mt-[20px]"
              src="https://i.pinimg.com/originals/1f/3f/4c/1f3f4ce973d946578567f190e2773709.png"
              alt="River"
            />
            <div className="px-6 py-4">
              <div className="font-bold text-center text-xl mb-2">TodoList</div>
              <p className="text-gray-700 text-center">
              Salah satu manfaatnya yakni bisa mengatur kegiatan sehari-hari. Kegiatan atau tugas mu akan lebih mudah untuk dikelola dan dikerjakan, setelah menulis dalam To Do List
              </p>
            </div>
          </a>
        </div>
      </div>

    
  </div>
</section>
    <footer className="container">
      <p>&copy; Regamahesa 2022</p>
    </footer>

    </div>
  )
}

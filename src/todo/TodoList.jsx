import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Table } from 'reactstrap';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';
import {Form, InputGroup } from "react-bootstrap";
import { Button,  Container, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import "../style/List.css";

function TodoList() {
  const [show, setShow] = useState(false);
  const [tugas, setTugas] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [list, setList] = useState([]);
  const [totalpages, setTotalPages] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useHistory();

  const addTugas = async (e) => {
    e.preventDefault();
    // e.persist();




    try {
      await axios.post(`http://localhost:3007/list?deskripsi=${deskripsi}&tugas=${tugas}&userId=` + localStorage.getItem("userId"),

        {
          tugas: tugas,
          deskripsi: deskripsi,
        }
      );

      //Sweet Alert
      setShow(false);
      Swal.fire({
        icon: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000)

    } catch (error) {
      console.log(error)
    }

    // try {
    //   await axios.post("http://localhost:8000/daftarMakanan", {
    //     name: name,
    //     deskripsi: deskripsi,
    //     harga: harga,
    //     image: image
    //   })
    //   Swal.fire({
    //     icon: 'success',
    //     title: 'Your work has been saved',
    //     showConfirmButton: false,
    //     timer: 1500
    //   })
    //   setTimeout(() => {
    //     window.location.reload();
    //     }, 1000)
    // } catch (error) {
    //   console.log(error)
    // }
  };

  const getAll = async (page = 0) => {
    await axios
      .get(`http://localhost:3007/list?page=${page}&userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        const pages = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          pages.push(index)
        }
        setTotalPages(pages)
        setList(res.data.data.content);
      }).catch((error) => {
        alert("Terjadi kesalahan " + error)
      })
  }

  const deleteList = async (id) => {
    // Async-await bisa dikatakan sebagai cara mudah menggunakan JavaScript Promise yang agak sulit dipahami.

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3007/list/" + id)
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )

      }
    }).then(() => {
      window.location.reload();
    });
  }

  useEffect(() => {
    getAll();
  }, [])

  return (
    <div className="container my-5 bs">
      <li className="nav-item list-none">
        <a
          className="inline-block rounded border border-indigo-600 bg-indigo-600 px-12 py-3 text-sm font-medium text-white hover:bg-transparent hover:text-indigo-600 focus:outline-none focus:ring active:text-indigo-500 btn"
          onClick={handleShow}
        >
          Rencana hari ini?
        </a>              </li>
      <br />
      <br />
      <Table>
        <thead>
          <tr>
            <th>No</th>
            <th>Tugas</th>
            <th>Deskripsi</th>
            <th>Action</th>
            <th>Cheklist</th>
          </tr>
        </thead>
        <tbody>
          {list.map((data, idx) => {
            return (
              <tr key={idx}>
                <td>{idx + 1}</td>
                <td>{data.tugas}</td>
                <td>{data.deskripsi}</td>
                <td class="px-4 py-2">
                  <a href={"/edit/" + data.id}>
                    <button
                      className="mx-1 btn btn-warning"
                      style={{ backgroundColor: "green" }} >
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                  | |
                  <button
                    className="mx-1 btn btn-danger"
                    style={{ backgroundColor: "red" }}
                    onClick={() => deleteList(data.id)}
                  >
                    <i class="fas fa-trash-alt"></i>
                  </button>
                </td>
                <td>
                  <label for="SelectAll" class="sr-only">Select All</label>

                  <input
                    type="checkbox"
                    id="SelectAll"
                    class="h-5 w-5 rounded border-gray-300"
                  />
                </td>
              </tr>

            )
          })}
        </tbody>
      </Table>
      <br />
      <br />
      <a className='btn btn-warning' style={{ textAlign: "center" }} aria-current="page" href="/home"><i class="fas fa-backward"></i> Kembali</a>
      <br />
      <br />
      <Container className="pag">
        <Pagination aria-label="Page navigation example">
          <PaginationItem>
            <PaginationLink
              first
              href="#"
            />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink previous />
          </PaginationItem>
          {
            totalpages.map((data, index) => (
              <PaginationItem key={index}>
                <PaginationLink onClick={() => getAll(data)}>
                  {data + 1}
                </PaginationLink>
              </PaginationItem>
            ))
          }

          <PaginationItem>
            <PaginationLink next />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink
              href="#"
              last
            />
          </PaginationItem>
        </Pagination>
      </Container>


      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Add Todo</Modal.Title>
        </Modal.Header>
        <form onSubmit={addTugas}>
          <Modal.Body>
            <div className="mb-3">
              <Form.Label>
                <strong>Tugas </strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control placeholder="Massukkan Tugas" value={tugas} onChange={(e) => setTugas(e.target.value)} />
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control placeholder="Massukkan Deskripsi" value={deskripsi} onChange={(e) => setDeskripsi(e.target.value)} />
              </InputGroup>
            </div>

          </Modal.Body>
          <Modal.Footer>
            <Button color="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button type="submit" color="primary">Save</Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
}

export default TodoList;
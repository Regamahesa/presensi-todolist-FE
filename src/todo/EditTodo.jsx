import axios from 'axios';
import Swal from 'sweetalert2';
import React, { useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap'
import { useHistory, useParams } from 'react-router-dom'

export default function EditTodo() {
    const param = useParams();  
    const [tugas, setTugas] = useState("");
    const [deskripsi, setDeskripsi] = useState("");

    const history = useHistory();

    useEffect(() => {
        axios.get("http://localhost:3007/list/" + param.id)
        .then((response) =>{
            const newList = response.data.data;
            setTugas(newList.tugas);
            setDeskripsi(newList.deskripsi);
        })
        .catch((error) => {
            alert("terjadi kesalahan sir " + error)
        })
    }, []);

    const submitActionHandler = async (e) => {
        e.preventDefault();
        Swal.fire({
                title: 'apakah yakin di edit datanya?',
                showCancelButton: true,
                confirmButtonText: 'Edit',
      }).then((result) => {
        if (result.isConfirmed) {
         axios.put("http://localhost:3007/list/" + param.id , 
       {
        tugas:tugas,
        deskripsi:deskripsi
          }
          ).then(() => {
            history.push("/todo");
            Swal.fire('Berhasil Mengedit!', '', 'success')
          }).catch((error) => {
            console.log(error);
          })
        }
      })
      }

  return (
    <div>
          <div className="edit mx-5">
        <div className="container my-5">
          <form onSubmit={submitActionHandler}>
            <div className="name mb-3">
              <Form.Label>
                <strong>Tugas</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Tugas"
                  value={tugas}
                  onChange={(e) => setTugas(e.target.value)}
                />
              </InputGroup>
            </div>
  
            <div className="place-of-birth mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Deskripsi"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)} />
              </InputGroup>
            </div>
            <div className="d-flex justify-conten-end align-items-center mt-2 text-blue-600">
              <button className="buton btn" >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

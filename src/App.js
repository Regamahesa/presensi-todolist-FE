import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import login from './component/Login';
import Profile from './component/Profile';
import register from './component/Register';
import Home from './pages/Home';
import Presensi from './presensi/Presensi';
import EditTodo from './todo/EditTodo';
import TodoFrom from './todo/TodoList';
import EditProfil from './view/EditProfil';

function App() {
  return (
    <div className="App">
    <BrowserRouter>
    <main>
      <Switch>
        <Route  path="/" component={login} exact/>
        <Route  path="/register" component={register} exact/>
        <Route  path="/home" component={Home} exact/>
        <Route path="/edit/:id" component={EditTodo} exact/>
        <Route path="/editProfil/:id" component={EditProfil} exact/>
        <Route  path="/todo" component={TodoFrom} exact/>
        <Route  path="/presensi" component={Presensi} exact/>
        <Route  path="/profil" component={Profile} exact/>
      </Switch>
    </main>
    </BrowserRouter>
    </div>
  );
}

export default App;
